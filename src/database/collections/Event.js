const AbstractCollection = require("../collection");
class Event extends AbstractCollection {

    // beter with IoC 
    constructor(db) {
        super(db);
    }

}
module.exports = function (db) {
    return new Event(db);
}
module.exports = class AbstractCollection {

    // Could be better implemented with a dependecy injection controller (IoC)
    constructor(db) {

        if (this.constructor == AbstractCollection) {
            throw new Error("Abstract classes can't be instantiated.");
        }

        this.collectionKey = this.constructor.name.toLowerCase();
        this.db = db;
    }

    // load collection object 
    load() {
        return this.db.loadCollection(this.collectionKey);
    }
    // save collection object 
    save(collection) {
        return this.db.saveCollection(this.collectionKey, collection);
    }

    // get collection documents 
    async get() {
        return (await this.load()).documents;
    }

    // get document by id 
    async getById(id) {
        return (await this.get()).find(document => document.id == id);
    }

    // insert new document 
    async insert(document) {
        let collection = await this.load();
        document.id = collection.lastIncrementalId++;
        collection.documents.push(document);
        return this.save(collection);
    }
    // update document by id 
    async update(id, document) {
        let collection = await this.load();
        let index = collection.documents.findIndex(element => element.id == id);
        if (index == -1) {
            throw new Error("Element doesn't exist ");
        }
        collection.documents[index] = {
            ...document,
            id: collection.documents[index].id // id is immutable 
        };
        return this.save(collection);
    }

    async delete(id) {

        let collection = await this.load();
        let index = collection.documents.findIndex(element => element.id == id);
        if (index == -1) {
            throw new Error("Element doesn't exist ");
        }
        collection.documents = collection.documents.filter((_, i) => i != index);
        return this.save(collection);
    }

}
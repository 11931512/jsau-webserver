const express = require('express')
const morgan = require('morgan');
const path = require('path');
const app = express()
const port = 3000
app.use(morgan('dev'));
app.use(express.urlencoded());
const nunjucks = require('nunjucks');
app.engine('html', nunjucks.render);
app.set('view engine', 'html');
app.set('views', 'src/views');

nunjucks.configure(['src/views'], {
  autoescape: true,
  express: app
})


let connectionString = path.resolve("db.json");
if (process.env.NODE_ENV == 'test') {
  connectionString = path.resolve("db.test.json")
}
let db = require("naive-file-database")(connectionString);
let eventCollection = require("./database/collections/Event")(db);

const winston = require('winston');

// utilisation d'un module externe 
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.Console({
      format: winston.format.simple(),
    })
  ],
});


app.get('/', async (req, res) => {

  let events = await eventCollection.get();
  res.render('index', { events });

})

app.get('/edit/:id', async (req, res) => {

  try {
    let id = req.params.id;
    let event = await eventCollection.getById(id);
    event.date = new Date(event.date);
    res.render('edit', { event });
  } catch (ex) { // if id doesn't exist redirect to error 
    logger.error(ex.message);
    res.redirect('/error');
  }

})


app.post('/edit', async (req, res) => {
  try {
    let event = { name: req.body.name, date: req.body.date };
    let id = req.body.id;
    await eventCollection.update(id, event);
    res.redirect('/success');
  } catch (ex) {
    logger.error(ex.message);
    res.redirect('/error');
  }
});


app.get('/delete/:id', async (req, res) => {

  try {
    let id = req.params.id;
    await eventCollection.delete(id);
    res.redirect('/success');
  } catch (ex) { // if id doesn't exist redirect to error 
    logger.error(ex.message);
    res.redirect('/error');
  }

})

app.get('/error', (req, res) => {
  res.render('error');
});
app.get('/success', (req, res) => {
  res.render('success');
});

app.get('/create', async (req, res) => {
  res.render('create');
});

app.post('/create', async (req, res) => {
  try {
    let event = { name: req.body.name, date: req.body.date };
    await eventCollection.insert(event);
    res.redirect('/success');
  } catch (ex) {
    logger.error(ex.message);
    res.redirect('/error');
  }
});

app.get('/info', (req, res) => {
  res.send('jsau-webserver-1.0.0');
})
app.use('/static', express.static(path.join(__dirname, 'public')))


const server = app.listen(port, () => {
  console.log(`Server listening on ${port} `);
})

module.exports = { server, app }
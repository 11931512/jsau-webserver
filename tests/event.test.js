const request = require('supertest')
const {app , server} = require('../src/index')

describe('Event endpoints', () => {
    it('should get an html page' , async () => {
        const res = await request(app)
            .get('/')            
            .expect('Content-Type', /html/)
    })

    it('should create an event successfully' , async () => {
        const res = await request(app)
            .post('/create')      
            .type('form')      
            .send({ name : "test event " , date : new Date() })
            .expect('Location' , '/success')
        
    })
    
});

afterAll(done => {
    server.close();
    done();
});